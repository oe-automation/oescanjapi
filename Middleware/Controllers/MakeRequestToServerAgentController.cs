﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using JBoxTesting_OE_Scanning.IJBoxBrokers;
using Newtonsoft.Json;

namespace Middleware.Controllers
{
    public class MakeRequestToServerAgentController : ApiController
    {
        // GET: api/MakeRequestToServerAgent
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/MakeRequestToServerAgent/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/MakeRequestToServerAgent
        public string Post([FromBody] dynamic jsonObject)
        {
            string clientId = jsonObject.clientId;
            string message = jsonObject.message;

            var broker = (JBoxBroker)HttpContext.Current.Application["Broker"];

            var result = broker.ClientSubscribes(clientId);

            return broker.MakeRequestToServerAgent(clientId, message);

           /* HttpClient client = new HttpClient();
            string path = "http://ec2-52-6-15-81.compute-1.amazonaws.com/host/api/Host";

            var stringContent = new StringContent("ethan", System.Text.Encoding.UTF8, "application/json");
            //HttpResponseMessage response = 
            var response = client.PostAsync(path, stringContent).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ToString();
                return data;
            }

            return null;*/
        }

       

        /*private async  Task<string> CallPost(string Key, string Message)
        {
            HttpClient client = new HttpClient();
            string path = "http://ec2-52-6-15-81.compute-1.amazonaws.com/host/api/Host";

            var stringContent = new StringContent("ethan", System.Text.Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(path, stringContent);
            if (response.IsSuccessStatusCode)
            {
                string data = await response.Content.ReadAsStringAsync();
                return data;
            }

            return null;
        }*/

        /*{
            try
            {
                // Log start.
                var OutputFile = "C:\\temp\\MiddlewareTestOutput.txt";
                File.AppendAllText(OutputFile, message + "\n");

                // Store broker.
                // var BrokerObj = (JBoxBroker)HttpContext.Current.Application["Broker"];
                var ResponseValue = JBoxBroker.MakeRequestToServerAgent(String.Empty, message);

                 var r = JBoxBroker.MakeRequestToServerAgent(String.Empty, message);

                // Log value to file.
                string JsonOut = JsonConvert.SerializeObject(ResponseValue);
                File.AppendAllText(OutputFile, 
             "Broker Sent Message: " + message + " OK!  --  " + 
                    "RESPONSE: " + JsonOut + "\n");

                // Return.
                return "Broker Sent Message: " + message + " OK!  --  " +
                       "RESPONSE: " + JsonOut + "\n";
            }
            catch (Exception ex)
            {
                // Return value
                var OutputFile = "C:\\temp\\MiddlewareTestOutput.txt";
                File.AppendAllText(OutputFile, "Broker Failed To Send Message!\n" + ex.Message + "\n");
                return "Broker Failed To Send: " + message + "\nERROR: " + ex.Message;
            }
        }
        */

        // PUT: api/MakeRequestToServerAgent/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/MakeRequestToServerAgent/5
        public void Delete(int id)
        {
        }
    }
}
