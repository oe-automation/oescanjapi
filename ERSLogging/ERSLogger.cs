﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ErrorReportingService.ERSDispatcher;
using ErrorReportingService.ERSLoggers;
using NLog;
using NLog.Config;

namespace ErrorReportingService.ERSLoggers
{
    public class ERSLogger
    {
        // Info about the output location
        public DateTime TimeStarted;    // Time the logger was launched.
        public Guid LoggerGUID;         // GUID Of the logger
        public string LoggerName;       // Name of the logger
        public string LoggerFile;       // Path of the logger file.
        public string OutputPath = @"C:\OPUS-IVS\ERSLogging\";

        // NLog objects
        internal Logger NLogger;                         // NLog object
        internal LoggingConfiguration LoggingConfig;     // Logging config item

        // Types of actions the logger does.
        internal LoggerStatus Status;
        internal LoggerOutputs OutputTypes;
        internal LoggerActions OutputActions;
        
        // ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Builds a new logger operation here.
        /// </summary>
        /// <param name="AppName"></param>
        protected ERSLogger(string AppName, LoggerActions ActionSet = LoggerActions.BASE_LOGGER)
        {
            // Store app name and such.
            this.LoggerName = AppName;
            this.OutputPath = Path.Combine(OutputPath, AppName);
            this.LoggerGUID = Guid.NewGuid();

            // Find if any loggers exist right now.
            var LoggersBack = ERSLoggerQueue.GetLoggers(AppName);
            if (!LoggersBack.Any(Obj => Obj.NameOfLogger == AppName)) { TimeStarted = DateTime.Now; }
            else { TimeStarted = LoggersBack.FirstOrDefault().TimeLaunched; }

            // Build output file name and path
            string TimeNowFormatted = TimeStarted.ToString("MMddyy_HH:mm:ss");
            string NameOfFile = this.LoggerName + "_" + TimeNowFormatted + ".log";
            this.LoggerFile = Path.Combine(OutputPath, NameOfFile);

            // Action setup
            this.OutputActions = ActionSet;
            this.OutputTypes =
                LoggerOutputs.CONSOLE_LOGGING |
                LoggerOutputs.FILE_LOGGING |
                LoggerOutputs.SQL_LOGGING;

            // Get the nlog config.
            LoggingConfig = new LoggingConfiguration();

            // Targets where to log to: File and Console
            var FileLogger = new NLog.Targets.FileTarget("FileLogger") { FileName = this.LoggerFile };
            var DBLogger = new NLog.Targets.ConsoleTarget("DatabaseLogger");

            // Rules for mapping loggers to targets            
            LoggingConfig.AddRule(LogLevel.Info, LogLevel.Fatal, FileLogger);
            LoggingConfig.AddRule(LogLevel.Info, LogLevel.Fatal, DBLogger);

            // Apply config and store logger object
            LogManager.Configuration = LoggingConfig;
            NLogger = LogManager.GetCurrentClassLogger();

            // Set status to idle.
            this.Status = LoggerStatus.WAITING;
        }
        /// <summary>
        /// ERS Constructor
        /// </summary>
        /// <param name="AppName">Name of the app calling this.</param>
        /// <param name="ActionSet">Type of actions/logging we do.</param>
        /// <returns></returns>
        public static ERSLogger CreateNewLogger(string AppName, LoggerActions ActionSet = LoggerActions.BASE_LOGGER)
        {
            // Store the logger here.
            ERSLogger LoggerOut = null;

            // If Base operations
            if (ActionSet == LoggerActions.BASE_LOGGER)
                LoggerOut = new ERSBaseLogger(AppName, ActionSet);

            // If Timer operations
            if (ActionSet == LoggerActions.TIMER_LOGGER)
                LoggerOut = new ERSTimerLogger(AppName, ActionSet);

            // Throw on null or add to list.
            if (LoggerOut == null) { throw new Exception("FAILED TO GENERATE A LOGGER FOR TYPE: " + ActionSet.ToString()); }

            // Return the logger and add the pool of loggers
            ERSLoggerQueue.AddLoggerToPool(LoggerOut);
            return LoggerOut;
        }


        /// <summary>
        /// Writes a log entry out.
        /// </summary>
        public virtual void WriteLogEntry(string LogEntry, LogLevel Level)
        {
            // Get the calling name and then write to the log file. 
            // Formatted date time included.
            string CallName = GetCallingClass(true);
            string FormattedLog = $"[{LoggerName}][{CallName}][TIME: {DateTime.Now.ToString("T")}] ::: {LogEntry}";
            this.NLogger.Log(Level, FormattedLog);
        }


        /// <summary>
        /// Writes all the logger info out to the current file.
        /// </summary>
        internal void StoreLoggerInfo()
        {
            this.NLogger.Log(LogLevel.Debug, $"LOGGER NAME: {this.LoggerName}");
            this.NLogger.Log(LogLevel.Debug, $"--> TIME STARTED:  {this.TimeStarted}");
            this.NLogger.Log(LogLevel.Debug, $"--> LOGGER STATUS: {this.Status}");
            this.NLogger.Log(LogLevel.Debug, $"--> LOGGER FILE:   {this.LoggerFile}");
            this.NLogger.Log(LogLevel.Debug, $"--> LOGGER PATH:   {this.OutputPath}");
            this.NLogger.Log(LogLevel.Debug, $"--> LOGGER OUTS:   {this.OutputTypes}");
            this.NLogger.Log(LogLevel.Debug, $"--> LOGGER ACTS:   {this.OutputActions}");
        }
        /// <summary>
       /// Gets the name of the calling method.
       /// </summary>
       /// <returns>String of the full method name.</returns>
        internal string GetCallingClass(bool SplitString = false)
        {
            // Setup values.
            string FullCallName;
            Type DeclaredType;
            int SkipFrames = 2;

            do
            {
                MethodBase method = new StackFrame(SkipFrames, false).GetMethod();
                DeclaredType = method.DeclaringType;
                if (DeclaredType == null)
                {
                    // Conditionals for special method calls.
                    // if (method.Name.ToLower().Contains("options")) { IsOptions = true; }
                    // if (method.Name.ToLower().Contains("api")) { IsApi = true; }

                    return method.Name;
                }
                SkipFrames++;
                FullCallName = DeclaredType.FullName;
            }
            while (DeclaredType.Module.Name.Equals("mscorlib.dll", StringComparison.OrdinalIgnoreCase));

            // Check for split values.
            if (!SplitString) { return FullCallName; }

            // Split and return now.
            var FullNameSplit = FullCallName.Split('.');
            FullCallName = FullNameSplit[FullNameSplit.Length - 1];

            // Return the name here.
            return FullCallName;
        }
    }
}
