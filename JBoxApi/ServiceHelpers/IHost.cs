﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JBoxApi.ServiceHelpers
{
    public interface IHost
    {
        void ReceiveMessage(string Message);
    }
}