﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JBoxTesting_OE_Scanning.ApiObjects;

namespace JBoxTesting_OE_Scanning.IJBoxBrokers
{
    public interface IJboxBroker
    {
        bool Register(string ArgString);
        ApiResponse MakeRequestToServerAgent(string Key);
    }
}