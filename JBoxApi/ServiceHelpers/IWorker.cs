﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JBoxApi.ServiceHelpers
{
    public interface IWorker
    {
        void ReceiveMessage(string MessageValue);      // Process incoming message
    }
}