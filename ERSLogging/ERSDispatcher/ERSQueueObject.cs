﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ErrorReportingService.ERSLoggers;

namespace ErrorReportingService.ERSDispatcher
{
    /// <summary>
    /// Class used to track all the ERS Loggers in the ERS Logger Queue.
    /// </summary>
    public class ERSQueueObject
    {
        // Basic props of any ERS Logging object.
        public Guid GUIDValue;
        public string NameOfLogger;
        public DateTime TimeLaunched;
        public ERSLogger ERSLoggerObj;
        public LoggerStatus LoggerStatus;

        // ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Builds a new ERS queue item
        /// </summary>
        /// <param name="LoggerItem">Logger to add to the queue.</param>
        public ERSQueueObject(ERSLogger LoggerItem)
        {
            // Set all the values here.
            this.ERSLoggerObj = LoggerItem;
            this.LoggerStatus = LoggerItem.Status;
            this.GUIDValue = LoggerItem.LoggerGUID;
            this.NameOfLogger = LoggerItem.LoggerName;
            this.TimeLaunched = LoggerItem.TimeStarted;
        }
    }
}
