﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using JBoxTesting_OE_Scanning.ApiObjects;

namespace JBoxTesting_OE_Scanning.IJBoxBrokers
{
    public class JBoxBroker : IJboxBroker
    {
        public Dictionary<string, IJBoxApiDelegate> ApiMapping;     // Mapping values for API
        public Dictionary<string, string> HeaderValues;             // Mapping of api header values.

        public JBoxBroker()
        {
            ApiMapping = new Dictionary<string, IJBoxApiDelegate>();
            HeaderValues = new Dictionary<string, string>();

            // Get pairings.
            HeaderValues = (ConfigurationManager.GetSection("XRapidHeaderValues") as Hashtable)
                .Cast<DictionaryEntry>()
                .ToDictionary(n => n.Key.ToString(), n => n.Value.ToString());
        }

        // ------------------------------------------------------------------------------

        public bool Register(string ArgString)
        {
            // Init a new API Delegate and add instance to mapping.
            var ApiDelegate = new JBoxApiDelegate();
            ApiMapping.Add(ArgString, (IJBoxApiDelegate)ApiDelegate);

            // Return here.
            return true;
        }

        public ApiResponse MakeRequestToServerAgent(string Key)
        {
            // Get APi Interface.
            var ApiInterface = ApiMapping.Where(DictEntry => 
                    DictEntry.Key == Key)
                    .FirstOrDefault();

            // Get the API XRapidHead
            string RemovedHttps = (Key.Substring(Key.IndexOf(':') + 3)).Split('/')[0];
            var XRapidHead = HeaderValues.Where(DictEntry =>
                    DictEntry.Value.Contains(RemovedHttps))
                    .FirstOrDefault();

            // Make the header here.
            var RequestHead = new ApiHeader();
            RequestHead.value = XRapidHead.Value;

            // Make the request here.
            var ApiCallResponse = ApiInterface.Value.MakeGETCall(Key, null, RequestHead);
            return ApiCallResponse;
        }
    }
}
