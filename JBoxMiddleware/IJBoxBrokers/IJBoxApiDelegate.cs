﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JBoxTesting_OE_Scanning.ApiObjects;

namespace JBoxTesting_OE_Scanning.IJBoxBrokers
{
    public interface IJBoxApiDelegate
    {
        ApiResponse MakeGETCall(string Url, ApiMessage MessageObject, ApiHeader HeaderValue);
        ApiResponse MakePOSTCall(string Url, ApiMessage MessageObject, ApiHeader HeaderValue);
        ApiResponse MakePUTCall(string Url, ApiMessage MessageObject, ApiHeader HeaderValue);
    }
}
