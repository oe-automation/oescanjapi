﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace JBoxApi.ServiceHelpers
{
    public class Worker : IWorker
    {
        private string Name = "";
        public string OutputFileName;

        public Worker(string Name)
        {
            this.Name = Name;
            this.OutputFileName = "C:\\temp\\HostTestOutput.txt"; 
        }

        public void ReceiveMessage(string MessageValue)
        {
            // Process message here.
            File.AppendAllText(OutputFileName, "NAME: " + Name + " | " + MessageValue);
        }
    }
}