﻿using JBoxApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Middleware
{
    public class DataAccessLayer
    {
        public bool AddDebugLogRecord(int clientId, int transactionId, DateTime timeStamp, bool isMiddleWare, string message)
        {


            // insert
            using (var db = new ServerBasedScanningEntities())
            {
                var dbgLog = db.Set<DebugLog>();
                dbgLog.Add(new DebugLog { ClientId = clientId, TransactionId= transactionId, TimeStamp = timeStamp, IsMiddleWare = isMiddleWare, Message = message });

                db.SaveChanges();
            }

            return true;
        }
    }
}