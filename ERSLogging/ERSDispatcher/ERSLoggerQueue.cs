﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ErrorReportingService.ERSLoggers;

namespace ErrorReportingService.ERSDispatcher
{
    public static class ERSLoggerQueue
    {
        // List of all logger items in  the pool.
        private static List<ERSQueueObject> LoggerPool = new List<ERSQueueObject>();

        // ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Adds a logger item to the pool of all loggers.
        /// </summary>
        /// <param name="LoggerItem">Item to add to the pool.</param>
        public static void AddLoggerToPool(ERSLogger LoggerItem)
        {
            // Make a new object.
            var NewLogger = new ERSQueueObject(LoggerItem);

            // Find existing loggers with the same name. Make sure this isn't a dupe.
            var SameNames = LoggerPool
                .Where(Obj => Obj.NameOfLogger == LoggerItem.LoggerName);

            // If the names match return since it exists already. Add to list if passed.
            if (!SameNames.Any(Obj => Obj.ERSLoggerObj.OutputActions == LoggerItem.OutputActions))
                LoggerPool.Add(NewLogger);
        }

        /// <summary>
        /// Gets a list of all pool loggers for the current app name.
        /// </summary>
        /// <param name="AppName">App calling this.</param>
        /// <returns>List of logging objects</returns>
        public static List<ERSQueueObject> GetLoggers(string AppName)
        {
            // Logger object to populate
            var Loggers = new List<ERSQueueObject>();
            Loggers.AddRange(LoggerPool
                .Where(Obj => Obj.NameOfLogger == AppName)
            );

            // Return the loggers found.
            return Loggers;
        }
    }
}
