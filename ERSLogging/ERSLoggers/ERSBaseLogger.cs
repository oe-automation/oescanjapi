﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ErrorReportingService;
using NLog;

namespace ErrorReportingService.ERSLoggers
{
    public class ERSBaseLogger : ERSLogger
    {
        /// <summary>
        /// CTOR For the ERS Logger object used for static init.
        /// </summary>
        /// <param name="AppName">Name of calling app</param>
        /// <param name="ActionSet">Action types</param>
        public ERSBaseLogger(string AppName, LoggerActions ActionSet = LoggerActions.BASE_LOGGER) : base(AppName, ActionSet)
        {
            // Set status to running.
            this.Status = LoggerStatus.RUNNING;
            this.NLogger.Log(LogLevel.Debug, "BASE LOGGER STARTED OK!");

            // Write logger infos out.
            base.StoreLoggerInfo();
        }

        // ---------------------------------------------------------------------------------------------

    }
}
