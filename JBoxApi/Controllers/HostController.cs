﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using JBoxApi.ServiceHelpers;

namespace JBoxApi.Controllers
{
    public class HostController : ApiController
    {
        // GET: api/Host
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Host/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Host
        public string Post([FromBody]string value)
        {
            try
            {
                // Log start.
                var OutputFile = "C:\\temp\\HostTestOutput.txt";
                File.AppendAllText(OutputFile, value + "\n");

                // Make host and process value
                var HostItem = new Host();
                HostItem.ReceiveMessage(value);

                // Return value
                File.AppendAllText(OutputFile, "Returned from broker host init OK!");
                return String.Format("{0}, with input value of {1} was SUCCESSFUL", "HostController.Post() : " , value);
            }
            catch (Exception ex)
            {
                // Log Error.
                var OutputFile = "C:\\temp\\HostTestOutput.txt";
                File.AppendAllText(OutputFile, ex.Message + "\n");

                // Return value
                File.AppendAllText(OutputFile, "Failed to init new host!\nERROR:" + ex.Message);
                return "Host Failed To Process: " + value + "\nERROR: " + ex.Message;
            }
        }

        // PUT: api/Host/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Host/5
        public void Delete(int id)
        {
        }
    }
}
