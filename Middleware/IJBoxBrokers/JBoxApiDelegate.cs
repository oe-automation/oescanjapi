﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using JBoxTesting_OE_Scanning.ApiObjects;
using RestSharp;

namespace JBoxTesting_OE_Scanning.IJBoxBrokers
{
    public class JBoxApiDelegate : IJBoxApiDelegate
    {
        public ApiResponse MakeGETCall(string Url, ApiMessage MessageObject, ApiHeader HeaderValue)
        {
            // Download the response from the RestSharp Client .
            var client = new RestClient(Url);
            var request = new RestRequest(Method.GET);

            // Get the Header URL Value.
            request.AddHeader("useQueryString", "true");
            request.AddHeader(HeaderValue.name, HeaderValue.value); 
            IRestResponse response = client.Execute(request);

            // Return a new API Response Object.
            return new ApiResponse(response.Content);
        }

        public async Task<ApiResponse> MakePOSTCall(string Url, ApiMessage MessageObject, ApiHeader HeaderValue)
        {
            using (var WebClient = new HttpClient())
            {
                // api/Register
                WebClient.BaseAddress = new Uri(Url);
                WebClient.DefaultRequestHeaders.Accept.Clear();
                WebClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Data output
                var Response = await WebClient.PostAsJsonAsync("api/Host", MessageObject.Message);
                string RespString = "STATUS: " + Response.StatusCode.ToString() + "\nCONTENT: " + Response.Content.ToString();
                var ResponseOut = new ApiResponse(RespString);

                // Return the output
                return ResponseOut;
            }
        }

        public ApiResponse MakePUTCall(string Url, ApiMessage MessageObject, ApiHeader HeaderValue)
        {
            // Temp Return.
            return new ApiResponse();
        }
    }
}
