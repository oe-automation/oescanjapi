﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using JBoxTesting_OE_Scanning.IJBoxBrokers;

namespace Middleware.Controllers
{
    public class RegisterController : ApiController
    {
        // GET: api/Register
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Register/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Register
        public string Post([FromBody] dynamic jsonObject)
        {
            string value = jsonObject.hostUrl;
            try
            {
                // Log start.
                var OutputFile = "C:\\temp\\MiddlewareTestOutput.txt";
                File.AppendAllText(OutputFile, value + "\n");

                // Store broker.
                var BrokerObj = (JBoxBroker) HttpContext.Current.Application["Broker"];
                BrokerObj.Register(value);

                // Return value.
                File.AppendAllText(OutputFile, "Broker Registered Host: " + value + " OK!\n");
                return "Broker Registered: " + value;
            }
            catch (Exception ex)
            {
                // Log Error.
                var OutputFile = "C:\\temp\\MiddlewareTestOutput.txt";
                File.AppendAllText(OutputFile, ex.Message + "\n");

                // Return value.
                File.AppendAllText(OutputFile, "Broker Failed To Register Host: " + value + "\nERROR: " + ex.Message);
                return "Broker Failed To Register: " + value + "\nERROR: " + ex.Message;
            }
        }

        // PUT: api/Register/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Register/5
        public void Delete(int id)
        {
        }
    }
}
