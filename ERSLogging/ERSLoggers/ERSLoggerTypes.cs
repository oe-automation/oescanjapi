﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErrorReportingService.ERSLoggers
{
    [Flags]
    public enum LoggerOutputs : uint
    {
        CONSOLE_LOGGING = 0x000001,
        FILE_LOGGING = 0x000002,
        SQL_LOGGING  = 0x00003,
    }

    [Flags]
    public enum LoggerActions : uint
    {
        BASE_LOGGER = 0x000001,
        TIMER_LOGGER = 0x000002,
    }

    [Flags]
    public enum LogEntryType : uint
    {
        LOG_INFO = 0x000001,
        LOG_WARN = 0x000002,
        LOG_FAIL = 0x000003,
    }

    [Flags]
    public enum LoggerStatus : uint
    {
        STOPPED = 0x000001,
        WAITING = 0x000002,
        RUNNING = 0x00003,
    }
}
