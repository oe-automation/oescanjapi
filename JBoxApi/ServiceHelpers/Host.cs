﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace JBoxApi.ServiceHelpers
{
    public class Host : IHost
    {
        private string Name;
        private string MiddleURL;
        private IWorker Worker;

        public Host()
        {
            // Make Name and middleware URL.
            this.Name = WebConfigurationManager.AppSettings["Name"];
            this.MiddleURL = WebConfigurationManager.AppSettings["MiddleWareURL"];

            // Make a new worker.
            this.Worker = new Worker(Name);

            // Build request to middleware.
            // InitHost(); 
        }


        /// <summary>
        /// Builds a middleware request for the host class init.
        /// </summary>
        public async void InitHost()
        {
            // Build request to the middleware API.
            HttpClient WebClient = new HttpClient();
            var RequestBody = new Dictionary<string, string> { { "Name", Name } };

            // Built request and the response values here.
            var RequestContent = new FormUrlEncodedContent(RequestBody);
            var ResponseOut = await WebClient.PostAsync(MiddleURL, RequestContent);
            var ResponseString = await ResponseOut.Content.ReadAsStringAsync();

            // Write response output.
            var OutputFile = "C:\\temp\\HostTestOutput.txt";
            File.AppendAllText(OutputFile, Name + " | " + ResponseString + "\n");
        }

        public void ReceiveMessage(string Message)
        {
            Worker.ReceiveMessage(Message);
        }
    }
}