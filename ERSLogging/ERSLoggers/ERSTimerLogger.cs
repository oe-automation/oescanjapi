﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace ErrorReportingService.ERSLoggers
{
    public class ERSTimerLogger : ERSLogger
    {
        public Stopwatch FullLoggerTimer;               // Logger timer for the whole app.
        public List<RecursiveTimers> StackTimerSet;     // Stacktrace to log out.

        public class RecursiveTimers
        {
            // Timer and names of the current stack.
            public string CurrentStack;
            public Stopwatch StackTimer;
            public List<RecursiveTimers> SubRoutineTimers;

            /// <summary>
            /// Builds a RecursiveTimer object
            /// </summary>
            /// <param name="NameOfStack">Name of the calling class</param>
            public RecursiveTimers(string NameOfStack)
            {
                // Timer setup and new list.
                this.StackTimer = new Stopwatch();
                this.SubRoutineTimers = new List<RecursiveTimers>();

                // Store name of stack
                this.CurrentStack = NameOfStack;
                this.StackTimer.Start();
            }
        }

        // ---------------------------------------------------------------------------------------------

        /// <summary>
        /// CTOR For the ERS Logger object used for static init.
        /// </summary>
        /// <param name="AppName">Name of calling app</param>
        /// <param name="ActionSet">Action types</param>
        public ERSTimerLogger(string AppName, LoggerActions ActionSet = LoggerActions.BASE_LOGGER) : base(AppName, ActionSet)
        {
            // Setup timers.
            this.StackTimerSet = new List<RecursiveTimers>();
            this.FullLoggerTimer = new Stopwatch();
            this.FullLoggerTimer.Start();

            // Set status to running.
            this.Status = LoggerStatus.RUNNING;
            this.NLogger.Log(LogLevel.Debug, "TIMER LOGGER STARTED OK!");

            // Write logger infos out.
            base.StoreLoggerInfo();
        }


        /// <summary>
        /// Writes a log file output to the given location
        /// </summary>
        /// <param name="LogEntry">Logs a set entry value</param>
        /// <param name="Level">Level of logging.</param>
        public override void WriteLogEntry(string LogEntry, LogLevel Level)
        {
            // Get the calling name and make a new timer object
            string CallNameOnly = GetCallingClass(true);
            var TimersWithMethod = StackTimerSet
                .Where(Obj => Obj.CurrentStack.EndsWith(CallNameOnly));

            // Make the next timer and format a string.
            var NextTimer = new RecursiveTimers(GetCallingClass());
            string FormattedLog = $"[{LoggerName}][{CallNameOnly}][TIME: {NextTimer.StackTimer}] ::: {LogEntry}";
            this.NLogger.Log(Level, FormattedLog);

            // If nothing exists add a new one in here.
            if (TimersWithMethod.Count() == 0) { StackTimerSet.Add(NextTimer); }
            else { TimersWithMethod.FirstOrDefault().SubRoutineTimers.Add(NextTimer); }
        }

        /// <summary>
        /// Call this to kill the timer session and stop tracking this stack trace.
        /// </summary>
        /// <param name="StackBase">Base method to search from</param>
        /// <param name="Level">Log Level</param>
        public void EndTimerSession(string StackBase, LogLevel Level)
        {
            // Get the timers with this stack parent.
            var TimersWithMethod = StackTimerSet
                .Where(Obj => Obj.CurrentStack.StartsWith(StackBase));

            // Print them out all to the log now.
            this.NLogger.Log(Level, $"[${LoggerName}] ::: ENDING DIAG TIMER SESSION FOR STACK {StackBase}");
            foreach (var StackTimerSet in TimersWithMethod)    
                WriteTimerEnds(StackTimerSet.CurrentStack, Level);
        }
        /// <summary>
        /// Writes the tiemr ends using recursive output.
        /// </summary>
        /// <param name="StackBase">Name to start looking for.</param>
        /// <param name="Level">Level of logging.</param>
        private void WriteTimerEnds(string StackBase, LogLevel Level)
        {
            // Get the timers with this stack parent.
            var TimersWithMethod = StackTimerSet
                .Where(Obj => Obj.CurrentStack.Contains(StackBase));

            // Loop all the values and write them out one by one.
            foreach (var StackTimerSet in TimersWithMethod)
            {
                // Write out the bottom set value.
                string BottomSet = StackTimerSet.CurrentStack;
                string TimePassed = StackTimerSet.StackTimer.Elapsed.ToString("g");
                this.NLogger.Log(Level, $"[{LoggerName}][PARENT: {StackBase}][BOTTOM: {BottomSet}] ::: ELAPSED: {TimePassed}");

                string NextBase = StackTimerSet.CurrentStack;
                if (StackTimerSet.SubRoutineTimers.Count != 0)
                    WriteTimerEnds(NextBase, Level);
            }
        }
    }
}
