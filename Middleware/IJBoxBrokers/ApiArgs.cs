﻿namespace JBoxTesting_OE_Scanning.ApiObjects
{
    public class ApiHeader
    {
        public string name = "x-rapidapi-host";
        public string value = "";
    }
    
    public class ApiMessage
    {
        public string Message;
    }

    public class ApiResponse
    {
        public string ResponseString;       // Response value from API Call.
        public ApiResponse(string Response = "")
        {
            if (string.IsNullOrEmpty(Response)) { return; }
            this.ResponseString = Response;
        }
    }
}