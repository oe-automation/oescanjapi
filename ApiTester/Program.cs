﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ApiTester
{
    class Program
    {
        static HttpClient client = new HttpClient();
        static string path = "http://ec2-52-6-15-81.compute-1.amazonaws.com/host/api/Host";


        static async Task Main(string[] args)
        {
            Console.WriteLine("Press enter to start test");
            Console.ReadLine();
            //string? data = await CallGet();
            string? data = await CallPost();


            Console.WriteLine(data ?? "No data returned");
            Console.ReadLine();
        }

        private static async Task<string?> CallGet()
        {
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                string data = await response.Content.ReadAsStringAsync();
                return data;
            }

            return null;
        }

        private static async Task<string?> CallPost()
        {
            var stringContent = new StringContent("ethan", System.Text.Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(path, stringContent);
            if (response.IsSuccessStatusCode)
            {
                string data = await response.Content.ReadAsStringAsync();
                return data;
            }

            return null;
        }
    }
}
