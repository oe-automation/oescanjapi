﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Newtonsoft.Json;

namespace JBoxApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            using (var WebClient = new HttpClient())
            {
                // api/Register
                WebClient.BaseAddress = new Uri("http://ec2-52-6-15-81.compute-1.amazonaws.com/middleware/");
                WebClient.DefaultRequestHeaders.Accept.Clear();
                WebClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Data output
                var Response = await WebClient.PostAsJsonAsync("api/Register", "http://ec2-52-6-15-81.compute-1.amazonaws.com/host/");
                string RespString = "STATUS: " + Response.StatusCode.ToString() + "\nCONTENT: " + Response.Content.ToString();
                Debug.WriteLine(RespString);

                var OutputFile = "C:\\temp\\HostTestOutput.txt";
                File.AppendAllText(OutputFile, RespString + "\n");
            }
        }
    }
}
