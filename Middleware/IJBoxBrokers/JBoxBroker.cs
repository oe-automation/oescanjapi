﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using JBoxTesting_OE_Scanning.ApiObjects;
using Newtonsoft.Json;

namespace JBoxTesting_OE_Scanning.IJBoxBrokers
{
    public class JBoxBroker 
    {
        #region Obsolete
        //OBSOLETE
        public Dictionary<string, IJBoxApiDelegate> ApiMapping;     // Mapping values for API
        public Dictionary<string, string> HeaderValues;             // Mapping of api header values.
        #endregion

        public Dictionary<string, string> RegisteredClients;
        public Dictionary<string, bool> RegisteredScanAgents;
        private string  OutputFile = "C:\\temp\\MiddlewareTestOutput.txt";


        public JBoxBroker()
        {
            ApiMapping = new Dictionary<string, IJBoxApiDelegate>();
            HeaderValues = new Dictionary<string, string>();

            RegisteredClients = new Dictionary<string, string>();
            RegisteredScanAgents  = new Dictionary<string, bool>();

            File.AppendAllText(OutputFile, DateTime.Now.ToString("s") + " | MIDDLEWARE - JBoxBroker Constructor\n\r");

            // Get pairings.
            // HeaderValues = (ConfigurationManager.GetSection("XRapidHeaderValues") as Hashtable)
            //     .Cast<DictionaryEntry>()
            //     .ToDictionary(n => n.Key.ToString(), n => n.Value.ToString());
        }

        // ------------------------------------------------------------------------------

        public bool Register(string agentApiUrl)
        {
            // Init a new API Delegate and add instance to mapping.
            //var ApiDelegate = new JBoxApiDelegate();
            //ApiMapping.Add(ArgString, string.Empty);


            RegisteredScanAgents.Add(agentApiUrl, false);
            // Return here.
            return true;
        }

        public bool ClientSubscribes(string clientId)
        {
            if (RegisteredClients.Where(w => w.Key.Equals(clientId)).Count().Equals(0))
            {

                var firstAvailable = RegisteredScanAgents.Where(w => w.Value.Equals(false)).FirstOrDefault();
                RegisteredScanAgents[firstAvailable.Key] = true;
                RegisteredClients.Add(clientId, firstAvailable.Key);
            }
            // Return here.
            return true;
        }

        #region Obsolete
        public ApiResponse MakeRequestToServerAgentV1(string clientId, string Message)
        {
            /*
            // Get a delegate
            var ApiInterface = RegisteredClients.Where(w => w.Key.Equals(clientId)).FirstOrDefault().Value;

            // Invoke the task 
            var ResponseTask = Task.Run(async () => await
                ApiInterface.Value.MakePOSTCall(
                    ApiInterface.Key,
                    new ApiMessage() { Message = Message },
                    new ApiHeader()
                ));

            // Return it here.
            */
            return null;
        }
        #endregion
        public  string MakeRequestToServerAgent(string clientId, string Message)
        {
            HttpClient client = new HttpClient();
            string path = RegisteredClients.Where(w => w.Key.Equals(clientId)).FirstOrDefault().Value;  /*"http://ec2-52-6-15-81.compute-1.amazonaws.com/host/api/Host";*/

            var stringContent = new StringContent(Message, System.Text.Encoding.UTF8, "application/json");
            var response = client.PostAsync(path, stringContent).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                return data;
            }

            return String.Empty;
        }


       /* private  async Task<string> CallPost(string clientId, string Message)
        {
            HttpClient client = new HttpClient();
            string path = RegisteredClients.Where(w => w.Key.Equals(clientId)).FirstOrDefault().Value;  *//*"http://ec2-52-6-15-81.compute-1.amazonaws.com/host/api/Host";*//*

            var stringContent = new StringContent(Message, System.Text.Encoding.UTF8, "application/json");
            var response = client.PostAsync(path, stringContent).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ToString();
                return data;
            }

            return null;
        }*/
    }
}
